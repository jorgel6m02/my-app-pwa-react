import { Typography } from "@mui/material";

export default function ProblemsPage() {
  return (
    <>
      <Typography variant="h2">
        Problems
      </Typography>
      <Typography variant="p">
        List of all created problems
      </Typography>
    </>
  );
}