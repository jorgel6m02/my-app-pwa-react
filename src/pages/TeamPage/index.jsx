import { Typography } from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

export default function TeamPage({context}) {
  let [team, setTeam] = useState({});
  let {teamId} = useParams();

  useEffect(() => {
    setTeam(context.retrieveOne(teamId));
  });

  return (
    <>
      <Typography variant="h3">Team Info</Typography>;
    </>
  );
}