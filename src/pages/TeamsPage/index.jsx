import { Typography, Grid, Avatar } from "@mui/material";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

export default function TeamsPage(props) {
  const [teams, setTeams] = useState([]);

  const gridItemStyles = {
    textAlign: "center",
  };

  useEffect(() => {
    setTeams(props.context.getAll());
  }, [])

  return (
    <>
      <Typography variant="h2">
        Teams
      </Typography>
      <Typography variant="p">
        List of all registered teams.
      </Typography>

      <Grid container spacing={3} mt={2}>
        {teams.map(team => {
          return (
            <Grid key={team.id} item xs={4} sx={gridItemStyles}>
              <Link to={`/team/${team.id}`}>
                <Avatar
                  alt={team.teamName}
                  src={team.image}
                  sx={{ width: 100, height: 100, margin: "auto" }}
                />
                <Typography variant="h5">{team.teamName}</Typography>
              </Link>
            </Grid>
          );
        })}
      </Grid>
    </>
  );
}