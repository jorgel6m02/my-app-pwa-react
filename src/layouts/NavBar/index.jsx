import { Grid, Button } from "@mui/material";
import { Download } from "@mui/icons-material";

import { Link } from "react-router-dom";
import { useEffect, useState } from "react";

export default function NavBar() {
  const [isReadyForInstall, setIsReadyForInstall] = useState(false);

  const gridItemStyles = {
    textAlign: "center"
  };

  useEffect(() => {
    window.addEventListener("beforeinstallprompt", (e) => {
      e.preventDefault();
      window.deferredPrompt = e;
      setIsReadyForInstall(true);
    });
  }, []);

  const downloadApp = () => {
    // const promptEvent = window.deferredPrompt;
    // if (!promptEvent) {
    //   console.error("No prompt was saved");
    //   return;
    // }
    // promptEvent.prompt();
    // const result = await promptEvent.userChoice;
    // console.log(result);
    // window.deferredPrompt = null;
    // setIsReadyForInstall(false);
    const promptEvent = window.deferredPrompt;
    promptEvent.prompt();
    promptEvent.userChoice
      .then((choiceResult) => {
        if (choiceResult.outcome === "accepted") {
          window.deferredPrompt = null;
          setIsReadyForInstall(false);
        }
      })
  }

  return (
    <>
      <Grid container spacing={4}>
        <Grid item xs={3} sx={gridItemStyles}>
          <Link to="/">Teams</Link>
        </Grid>
        <Grid item xs={3} sx={gridItemStyles}>
          <Link to="/problems">Problems</Link>
        </Grid>
        <Grid item xs={3} sx={gridItemStyles}>Clarifications</Grid>
        {isReadyForInstall && (
          <Grid item xs={3} sx={gridItemStyles}>
            <Button onClick={downloadApp} variant="outlined" startIcon={<Download />}>
              Download PWA
            </Button>
          </Grid>
        )}
      </Grid>
    </>
  );
}