import { Routes, Route, useParams } from 'react-router-dom';

import NavBar from './layouts/NavBar';
import TeamsPage from './pages/TeamsPage';
import TeamPage from './pages/TeamPage';
import ProblemsPage from './pages/ProblemsPage';

import StorageDB from "./utils/db-storage";
import { teams } from './enums/team-data';
import { problems } from './enums/problem-data';
import TeamService from './services/team-service';

function App() {
  let storageName = "react-app";
  let db = new StorageDB(storageName);
  let dbData = {
    teams,
    problems,
  };
  db.init(dbData);
  let teamsService = new TeamService(db.getKeyCollectionAsJson("teams"));

  return (
    <>
      <NavBar />
      <Routes>
        <Route path="/" element={<TeamsPage context={teamsService} />}></Route>
        <Route path="/problems" element={<ProblemsPage />}></Route>
        <Route path="/team/:teamId" element={<TeamPage context={teamsService} />}></Route>
      </Routes>
    </>
  );
}

export default App;
