export default class StorageDb {
  constructor(storageName) {
    this.storageName = storageName;
  }

  init(jsonData = null) {
    if (this.storageName !== null && jsonData !== null) {
      if (!this.exists()) {
        let stringData = JSON.stringify(jsonData);
        localStorage.setItem(this.storageName, stringData);
      }
    }
  }

  exists() {
    return localStorage.hasOwnProperty(this.storageName);
  }

  getKeyCollectionAsJson(keyName) {
    let stringData = localStorage.getItem(this.storageName);
    let parsedData = JSON.parse(stringData);
    return parsedData[keyName];
  }
}
