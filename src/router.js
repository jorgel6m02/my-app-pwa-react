import { createBrowserRouter } from "react-router-dom";

import TeamsPage from "./pages/TeamsPage";
import ProblemsPage from "./pages/ProblemsPage";

const router = createBrowserRouter([
  {
    path: "/",
    element: <TeamsPage />
  },
  {
    path: "/problems",
    element: <ProblemsPage />
  }
]);

export default router;