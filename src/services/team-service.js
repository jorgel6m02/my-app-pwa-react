export default class TeamService {
  constructor(collection) {
    this.collection = collection;
  }

  getAll() {
    return this.collection;
  }

  retrieveOne(id) {
    let foundTeam = this.collection.find(team => team.id === +id);
    return foundTeam;
  }
}