export const problems = [
  {
    id: 1,
    alias: "sum",
    title: "Sum Two Numbers From String",
    description: "Sum two numbers given a string",
    example: "Input: 2 + 4\nOutput: 6",
    points: 6,
    programmingLanguage: "python"
  },
  {
    id: 2,
    alias: "array-diff",
    title: "Array Difference",
    description: "Find the array difference between two same length arrays",
    example: "First Array Length: 2\nFirst Array Element: 1\nFirst Array Element: 2\Second Array Length: 2\Second Array Element: 3\nSecond Array Element: 1",
    points: 4,
    programmingLanguage: "java"
  }
]