export const teams = [
  {
    id: 1,
    teamName: "devteam",
    fullName: "Development Team",
    lastLogin: "2023-07-21T19:43:03.245Z",
    enabled: 0,
    image: "https://bcw.blob.core.windows.net/public/img/thedevteam-logo.png",
  },
  {
    id: 2,
    teamName: "united4sft",
    fullName: "UnitedxForxSoftware",
    lastLogin: "2023-07-21T19:43:03.245Z",
    enabled: 0,
    image: "https://images.genius.com/b736416d7879d78ed1a3ee5964a767fe.490x500x1.jpg",
  },
  {
    id: 3,
    teamName: "team4code",
    fullName: "Team 4 Code",
    lastLogin: "2023-07-21T19:43:03.245Z",
    enabled: 0,
    image: "https://www.team4code.com/wp-content/uploads/2020/03/Team4Code-Logo.svg",
  },
];